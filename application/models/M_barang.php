<?php

class M_barang extends CI_Model{
    function get_data_all(){
        $this->db->select('id,nama_barang,jumlah,status_barang'); //kalo mau panggil semua pakai "*"
        $this->db->from('master_barang'); //call table name
        //$this->db->where(array('status_barang'=>'Y'));
        $query = $this->db->get()->result_array();
//        echo "<pr>"; 
//        var_dump($query);die;
        return $query;
    }
    
    public function get_edit_data($id){
        $this->db->select('*');
        $this->db->from('master_barang');
        $this->db->where(array('id'=>$id));
        $query = $this->db->get()->row_array();
        return $query;
    }
}