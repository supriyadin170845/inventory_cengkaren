<?php

class M_supplier extends CI_Model{
    function get_data_all(){
        $this->db->select('*'); //kalo mau panggil semua pakai "*"
        $this->db->from('master_supplier'); //call table name
        //$this->db->where(array('status_barang'=>'Y'));
        $query = $this->db->get()->result_array();
//        echo "<pr>"; 
//        var_dump($query);die;
        return $query;
    }
    
    public function get_edit_data($id){
        $this->db->select('*');
        $this->db->from('master_supplier');
        $this->db->where(array('id'=>$id));
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function generate_code($tables, $prefix, $separator, $digit = 4, $field='id') {

        $this->db->select_max($field, 'max_id');
        
        $maxi = $this->db->get($tables)->row('max_id');
        
        $hsl = str_pad(($maxi == 0 ? 1 : intval($maxi) + 1), $digit, '0', STR_PAD_LEFT);
        
        return $prefix . $separator . $hsl;
    }

    public function countdata(){
        return $this->db->count_all('master_supplier');

    }

    public function getdata($limit,$pg){
        $this->db->select('*');
        $this->db->from('master_supplier');
        $this->db->limit($pg,$limit);
        return $this->db->get()->result_array();   
    }
    
}