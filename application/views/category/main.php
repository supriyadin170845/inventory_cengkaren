<div class="row">
    <div class="col-lg-12" style="padding-bottom: 5px;">
        <a href="<?php echo base_url('category/add');?>" class="btn btn-danger">ADD</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($all_data as $k=>$v){ 
                    if($v['status_category']=="Y"){
                        $status = "Aktif";
                    }else{
                        $status = "Non Aktif";
                    }
                ?>
                <tr>
                    <td><?php echo $v['id'];?></td>
                    <td><?php echo $v['nama_category'];?></td>
                    <td><?php echo $status;?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo base_url('category/edit/'.$v['id']);?>" class="btn btn-succes btn-sm">EDIT</a> 
                        <a href="<?php echo base_url('category/delete/'.$v['id']);?>" class="btn btn-info btn-sm" onclick="return confirm('Yakin Hapus Data ?');">DELETE</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>