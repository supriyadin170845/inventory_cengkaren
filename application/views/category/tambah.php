<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Form Tambah Kategori</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('category/ADD_save'); ?>">
                    <div class="form-group">
                        <label>Nama Kategori</label>
                        <input type="text" name="nama_category" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Status Kategori</label>
                        <select name="status_category" class="form-control">
                            <option value="Y">Aktif</option>
                            <option value="N">Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-warning" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

