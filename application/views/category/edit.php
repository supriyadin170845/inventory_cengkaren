<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Perubahan Kategori</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('category/edit_save'); ?>">
                    <div class="form-group">
                        <label>Nama Kategori</label>
                        <input type="hidden" name="id" value="<?php echo $detail['id'];?>">
                        <input type="text" name="nama_category" value="<?php echo $detail['nama_category'];?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Status category</label>
                        <select name="status_category" class="form-control">
                            <option value="Y" <?php echo ($detail['status_category']=="Y"?"selected":"");?>>Aktif</option>
                            <option value="N" <?php echo ($detail['status_category']=="N"?"selected":"");?>>Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-Success" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>