<div class="row"> 
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Form Tambah Supplier</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('supplier/add_save'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Kode supplier</label>
                        <input type="text" name="kode_supplier" value="<?php echo $kode_supplier;?>" required="true" readonly="true" class="form-control">
                    </div>
					<div class="form-group">
                        <label>Nama supplier</label>
                        <input type="text" name="nama_supplier" required="true" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Alamat supplier</label>
                        <textarea name="alamat_supplier" required="true" class="form-control"></textarea>
                    </div>
					<div class="form-group">
                        <label>Telepon supplier</label>
                        <input type="text" name="telepon_supplier" required="true" class="form-control">
                    </div>
					<div class="form-group">
                        <label>Email supplier</label>
                        <input type="email" name="email_supplier" required="true" class="form-control">
                    </div>
					<div class="form-group">
                        <label>Logo Supplier</label>
                        <input type="file" name="image_supplier" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Status supplier</label>
                        <select name="status_supplier" required="true" class="form-control">
                            <option value="Y">Aktif</option>
                            <option value="N">Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-primary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>