<div class="row">
    <div class="col-lg-12" style="padding-bottom: 5px;">
        <a href="<?php echo base_url('supplier/add');?>" class="btn btn-primary">ADD</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Telepon</th>
					<th>Email</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $k=>$v){ 
                    if($v['status_supplier']=="Y"){
                        $status = "Aktif";
                    }else{
                        $status = "Non Aktif";
                    }
                ?>
                <tr>
                    <td><?php echo $v['kode_supplier'];?></td>
                    <td><?php echo $v['nama_supplier'];?></td>
                    <td><?php echo $v['alamat_supplier'];?></td>
                    <td><?php echo $v['telepon_supplier'];?></td>
					<td><?php echo $v['email_supplier'];?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo base_url('supplier/edit/'.$v['id']);?>" class="btn btn-warning btn-sm">Edit</a> 
                        <a href="<?php echo base_url('supplier/delete/'.$v['id']);?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus Data ?');">DELETE</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div class="col-lg-2">   
            Total : <?php echo $total_rows; ?>
        </div>
        <div class="col-lg-10 pull-right text-right">   
            <?php echo $paging; ?>
        </div>
    </div>
</div>