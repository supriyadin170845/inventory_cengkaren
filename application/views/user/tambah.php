<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Form User</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('user/add_save'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Email *</label>
                        <input type="email" name="email" class="form-control" autofocus placeholder="Ex: name@gmail.com" required="true">
                    </div>
                    <div class="form-group">
                        <label>Password *</label>
                        <input type="password" name="password" class="form-control" required="true">
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" placeholder="Ex: Budiman" required="true">
                    </div>
                    <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control" required="true">
                            <option value="L">PRIA</option>
                            <option value="W">WANITA</option>
                        </select>
                    </div>
					<div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control" placeholder="Ex: Budiman" required="true">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control" required="true">
                            <option value="Y">Aktif</option>
                            <option value="N">Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-primary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>