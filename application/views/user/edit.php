<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Form Edit User</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('user/edit_save'); ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="hidden" name="id" value="<?php echo $detail['id'];?>">
                        <input type="email" name="email" value="<?php echo $detail['email'];?>" class="form-control" required="true" readonly="true">
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="password" name="password" class="form-control" value="<?php echo $detail['password'];?>" required="true">
                    </div>
                     <div class="form-group">
                        <label>nama</label>
                        <input type="text" name="nama" class="form-control" value="<?php echo $detail['nama'];?>" required="true">
                    </div>
                    
                    <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control" required="true">
							<?php 
								if($detail['gender']=="L"){
									$pria 	= "selected";
									$wanita = "";
								}else{
									$pria 	= "";
									$wanita = "selected";
								}
							?>
                            <option value="L" <?php echo $pria;?>>PRIA</option>
                            <option value="W" <?php echo $wanita;?>>WANITA</option>
                        </select>
                    </div>
					
					<div class="form-group">
                        <label>Image</label>
						<input type="" name="image_hidden" value="<?php echo $detail['image'];?>">
                        <input type="file" name="image" class="form-control">
                    </div>
					
                     <div class="form-group">
                        <label>status</label>
                        <select name="status" class="form-control">
                            <option value="Y" <?php echo ($detail['status']=="Y"?"selected":"");?>>Aktif</option>
                            <option value="N" <?php echo ($detail['status']=="N"?"selected":"");?>>Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-primary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>