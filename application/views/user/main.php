<div class="row">
    <div class="col-lg-12" style="padding-bottom: 5px;">
        <a href="<?php echo base_url('user/add');?>" class="btn btn-primary">ADD</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <!--<th>Password</th>-->
                    <th>Nama</th>
                    <th>Gender</th>
                    <th>Status</th>
					<th>Image</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($all_data as $k=>$v){ 
                    if($v['status']=="Y"){
                        $status = "Aktif";
                    }else{
                        $status = "Non Aktif";
                    }
					
					if($v['gender']=="L"){
                        $gender = "Pria";
                    }else{
                        $gender = "Wanita";
                    }
                ?>
                <tr>
                    <td><?php echo $v['id'];?></td>
                    <td><?php echo $v['email'];?></td>
                    <!--<td><?php echo $v['password'];?></td>-->
                    <td><?php echo $v['nama'];?></td>
                    <td><?php echo $gender;?></td>
                    <td><?php echo $status;?></td>
					<td>
						<a target="_blank" href="<?php echo base_url('assets/images/foto/'.$v['image']);?>">
							<img style="width:50px;height:50px;" src="<?php echo base_url('assets/images/foto/'.$v['image']);?>">
						</a>
					</td>
                    <td style="text-align: center;">
                        <a href="<?php echo base_url('user/edit/'.$v['id']);?>" class="btn btn-warning btn-sm">EDIT</a> 
                        <a href="<?php echo base_url('user/delete/'.$v['id']);?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus Data ?');">DELETE</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>