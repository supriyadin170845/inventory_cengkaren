<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Form Edit Barang</div>
            <div class="panel-body">
                <form method="post" name="form1" action="<?php echo base_url('barang/edit_save'); ?>">
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="hidden" name="id" value="<?php echo $detail['id'];?>">
                        <input type="text" name="nama_barang" value="<?php echo $detail['nama_barang'];?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jumlah Barang</label>
                        <input type="text" name="jumlah" class="form-control" value="<?php echo $detail['jumlah'];?>">
                    </div>
                    <div class="form-group">
                        <label>Status Barang</label>
                        <select name="status_barang" class="form-control">
                            <option value="Y" <?php echo ($detail['status_barang']=="Y"?"selected":"");?>>Aktif</option>
                            <option value="N" <?php echo ($detail['status_barang']=="N"?"selected":"");?>>Non Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-primary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>