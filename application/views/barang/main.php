<div class="row">
    <div class="col-lg-12" style="padding-bottom: 5px;">
        <a href="<?php echo base_url('barang/add');?>" class="btn btn-primary">ADD</a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Jumlah</th>
                    <th>Status</th>
                    <th style="text-align: center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($all_data as $k=>$v){ 
                    if($v['status_barang']=="Y"){
                        $status = "Aktif";
                    }else{
                        $status = "Non Aktif";
                    }
                ?>
                <tr>
                    <td><?php echo $v['id'];?></td>
                    <td><?php echo $v['nama_barang'];?></td>
                    <td><?php echo $v['jumlah'];?></td>
                    <td><?php echo $status;?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo base_url('barang/edit/'.$v['id']);?>" class="btn btn-warning btn-sm">main</a> 
                        <a href="<?php echo base_url('barang/delete/'.$v['id']);?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Hapus Data ?');">DELETE</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>