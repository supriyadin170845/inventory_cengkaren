<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class category extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('M_category');//untuk load model yang akan digunakan
    }
    
    public function index(){
        $data['all_data'] = $this->M_category->get_data_all();
        $data['title'] = "category";
        $data['view']='category/main';
        $this->load->view('template',$data);
    }
    
    public function add(){
        $data['title'] = "Add category";
        $data['view']='category/tambah';
        $this->load->view('template',$data);
    }
    
    public function edit($id){
        $data['detail'] = $this->M_category->get_edit_data($id);
        $data['title'] = "Edit category";
        $data['view']='category/edit';
        $this->load->view('template',$data);
    }
    
    public function add_save(){
        $nama_category = $this->input->post('nama_category');
        $status_category = $this->input->post('status_category');
        
        $data = array(//array adalah standart dari CI
            'nama_category'=>$nama_category,
            'status_category'=>$status_category
        );
        
        $this->db->insert('master_category',$data);
        redirect('category/index');
    }
    
    public function edit_save(){
        $id = $this->input->post('id');
        $nama_category = $this->input->post('nama_category');
        $status_category = $this->input->post('status_category');
        
        $data = array(
            'nama_category'=>$nama_category,
            'status_category'=>$status_category
        );
        
        $this->db->update('master_category',$data,array('id'=>$id));
        redirect('category/index');
    }
    
    public function delete($id){
        $this->db->delete('master_category',array('id'=>$id));
        redirect('category/index');
    }
}