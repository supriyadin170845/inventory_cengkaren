<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('M_supplier');//untuk load model yang akan digunakan
    }
    
    public function index(){
        $config['base_url'] = base_url() . 'supplier/index/';
        $config['total_rows'] = $this->M_supplier->countdata();
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $limit = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->pagination->initialize($config);
        $data['total_rows'] = $this->M_supplier->countdata();
        $data['paging'] = $this->pagination->create_links();
        $data['data'] = $this->M_supplier->getdata($limit, $config['per_page']);
        $data['title'] = "Supplier";
        $data['view']='Supplier/main';
        $this->load->view('template',$data);
    }
    
    public function add(){
        $data['title'] = "Add Supplier";
        $data['kode_supplier'] = $this->M_supplier->generate_code('master_supplier', 'SPL/CKI/'.date('m').'/'.date('y'), '/',5,'id');
        $data['view']='Supplier/tambah';
        $this->load->view('template',$data);
    }
    
    public function edit($id){
        $data['detail'] = $this->M_supplier->get_edit_data($id);
        $data['title'] = "Edit Supplier";
        $data['view']='Supplier/edit';
        $this->load->view('template',$data);
    }
    
    public function add_save(){
		
		$folder = "foto";
        if (!is_dir('./assets/images/' . $folder)) {
            mkdir('./assets/images/' . $folder, 0777, TRUE);
        }
		//upload File
        $config['upload_path'] = './assets/images/' . $folder;
        $config['upload_url']	= './assets/images/' . $folder;
        $config['allowed_types']= 'jpg|png|gif';
        $config['max_size']     = '5000';
        $config['remove_spaces']= TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if($this->upload->do_upload('image_supplier')){
            $img = $this->upload->data();    
            $img_name = $img['file_name'];
        }else{
            echo "<script>alert(\"File gagal di upload \"); window.history.back();</script>";
			die;
        }

        $kode_supplier = $this->input->post('kode_supplier');
        $nama_supplier = $this->input->post('nama_supplier');
        $alamat_supplier = $this->input->post('alamat_supplier');
        $telepon_supplier = $this->input->post('telepon_supplier');
        $email_supplier = $this->input->post('email_supplier');
        $status_supplier = $this->input->post('status_supplier');
        
        $data = array(//array adalah standart dari CI
            'kode_supplier'=>$kode_supplier,
            'nama_supplier'=>$nama_supplier,
            'alamat_supplier'=>$alamat_supplier,
            'telepon_supplier'=>$telepon_supplier,
			'email_supplier'=>$email_supplier,
			'status_supplier'=>$status_supplier,
			'image_supplier'=>$img_name
        );
        
        $this->db->insert('master_supplier',$data);
        redirect('Supplier/index');
    }
    
    public function edit_save(){
		
		$folder = "foto";
        if (!is_dir('./assets/images/' . $folder)) {
            mkdir('./assets/images/' . $folder, 0777, TRUE);
        }
		//upload File
        $config['upload_path'] = './assets/images/' . $folder;
        $config['upload_url']	= './assets/images/' . $folder;
        $config['allowed_types']= 'jpg|png|gif';
        $config['max_size']     = '2000';
        $config['remove_spaces']= TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if($this->upload->do_upload('image_supplier')){
            $img = $this->upload->data();    
            $img_name = $img['file_name'];
        }else{
            $img_name = $this->input->post('image_hidden');
        }
		
        $id = $this->input->post('id');
        $nama_supplier = $this->input->post('nama_supplier');
        $alamat_supplier = $this->input->post('alamat_supplier');
        $telepon_supplier = $this->input->post('telepon_supplier');
		$email_supplier = $this->input->post('email_supplier');
		$status_supplier = $this->input->post('status_supplier');
        
        $data = array(//array adalah standart dari CI
            'nama_supplier'=>$nama_supplier,
            'alamat_supplier'=>$alamat_supplier,
            'telepon_supplier'=>$telepon_supplier,
			'email_supplier'=>$email_supplier,
			'status_supplier'=>$status_supplier,
			'image_supplier'=>$img_name
        );
        
        $this->db->update('master_supplier',$data,array('id'=>$id));
        redirect('Supplier/index');
    }
    
    public function delete($id){
        $this->db->delete('master_supplier',array('id'=>$id));
        redirect('Supplier/index');
    }
}