<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('M_barang');//untuk load model yang akan digunakan
    }
    
    public function index(){
        $data['all_data'] = $this->M_barang->get_data_all();
        $data['title'] = "Barang";
        $data['view']='barang/main';
        $this->load->view('template',$data);
    }
    
    public function add(){
        $data['title'] = "Add Barang";
        $data['view']='barang/tambah';
        $this->load->view('template',$data);
    }
    
    public function edit($id){
        $data['detail'] = $this->M_barang->get_edit_data($id);
        $data['title'] = "Edit Barang";
        $data['view']='barang/edit';
        $this->load->view('template',$data);
    }
    
    public function add_save(){
        $nama_barang = $this->input->post('nama_barang');
        $jumlah = $this->input->post('jumlah');
        $status_barang = $this->input->post('status_barang');
        
        $data = array(//array adalah standart dari CI
            'nama_barang'=>$nama_barang,
            'jumlah'=>$jumlah,
            'status_barang'=>$status_barang
        );
        
        $this->db->insert('master_barang',$data);
        redirect('barang/index');
    }
    
    public function edit_save(){
        $id = $this->input->post('id');
        $nama_barang = $this->input->post('nama_barang');
        $jumlah = $this->input->post('jumlah');
        $status_barang = $this->input->post('status_barang');
        
        $data = array(
            'nama_barang'=>$nama_barang,
            'jumlah'=>$jumlah,
            'status_barang'=>$status_barang
        );
        
        $this->db->update('master_barang',$data,array('id'=>$id));
        redirect('barang/index');
    }
    
    public function delete($id){
        $this->db->delete('master_barang',array('id'=>$id));
        redirect('barang/index');
    }
}