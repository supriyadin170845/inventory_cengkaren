<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('M_user');//untuk load model yang akan digunakan
    }
    
    public function index(){
        $data['all_data'] = $this->M_user->get_data_all();
        $data['title'] = "user";
        $data['view']='user/main';
        $this->load->view('template',$data);
    }
    
    public function add(){
        $data['title'] = "Add user";
        $data['view']='user/tambah';
        $this->load->view('template',$data);
    }
    
    public function edit($id){
        $data['detail'] = $this->M_user->get_edit_data($id);
        $data['title'] = "Edit user";
        $data['view']='user/edit';
        $this->load->view('template',$data);
    }
    
    public function add_save(){
		$folder = "foto";
        if (!is_dir('./assets/images/' . $folder)) {
            mkdir('./assets/images/' . $folder, 0777, TRUE);
        }
		//upload File
        $config['upload_path'] = './assets/images/' . $folder;
        $config['upload_url']	= './assets/images/' . $folder;
        $config['allowed_types']= 'jpg|png|gif';
        $config['max_size']     = '2000';
        $config['remove_spaces']= TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if($this->upload->do_upload('image')){
            $img = $this->upload->data();    
            $img_name = $img['file_name'];
        }else{
            echo "<script>alert(\"File gagal di upload\"); window.history.back();</script>";
			die;
        }
	
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $nama = $this->input->post('nama');
        $gender= $this->input->post('gender');
        $status= $this->input->post('status');

		$validasi_email = $this->M_user->check_mail($email);
		if ( $validasi_email < 1 ) {
        $data = array(//array adalah standart dari CI
            'email'=>$email,
            'password'=>md5($password),
            'nama'=>$nama,
            'gender'=>$gender,
            'status'=>$status,
			'image'=>$img_name
         );
			$this->db->insert('master_user',$data);
			redirect('user/index');
		} else {
			echo "<script>alert(\"Email sudah terdaftar\"); window.history.back();</script>";
		}
    }
    
    public function edit_save(){
		$folder = "foto";
        if (!is_dir('./assets/images/' . $folder)) {
            mkdir('./assets/images/' . $folder, 0777, TRUE);
        }
		//upload File
        $config['upload_path'] = './assets/images/' . $folder;
        $config['upload_url']	= './assets/images/' . $folder;
        $config['allowed_types']= 'jpg|png|gif';
        $config['max_size']     = '2000';
        $config['remove_spaces']= TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if($this->upload->do_upload('image')){
            $img = $this->upload->data();    
            $img_name = $img['file_name'];
        }else{
            $img_name = $this->input->post('image_hidden');
        }
		
		$id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $nama = $this->input->post('nama');
        $gender= $this->input->post('gender');
        $status= $this->input->post('status');
        
		//$validasi_email = $this->M_user->check_mail($email);
		//if ( $validasi_email < 1 ) {
			
			$data = array(//array adalah standart dari CI
				'email'=>$email,
				'password'=>$password,
				'nama'=>$nama,
				'gender'=>$gender,
				'status'=>$status,
				'image'=>$img_name
		   
			);
			$this->db->update('master_user',$data,array('id'=>$id));
			redirect('user/index');
			
		//} else {
			//echo "<script>alert(\"Email sudah terdaftar\"); window.history.back();</script>";
		//}
    }
    
    public function delete($id){
        $this->db->delete('master_user',array('id'=>$id));
        redirect('user/index');
    }
}